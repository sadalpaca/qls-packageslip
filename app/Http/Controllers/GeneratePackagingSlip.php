<?php

namespace App\Http\Controllers;

use App\Services\PackingSlip\Builders\PackageSlipBuilder;
use Gotenberg\Gotenberg;
use Gotenberg\Stream;
use Http\Adapter\Guzzle7\Client;
use Illuminate\Http\Request;

class GeneratePackagingSlip extends Controller
{
    public function __invoke(Request $request, PackageSlipBuilder $builder)
    {

        $slip = $builder->build();

        $request = Gotenberg::chromium('http://gotenberg:3000')
            ->pdf()
            ->paperSize(8.27, 11.7)
            ->html(
                Stream::string('package-slip.html',
                    view('pdf', [
                        'order' => $slip['order'],
                        'shipment' => $slip['shipment'],
                        'image' => $slip['image'],
                    ])
                )
            );

        $client = new Client();

        return $client->sendRequest($request);
    }
}
