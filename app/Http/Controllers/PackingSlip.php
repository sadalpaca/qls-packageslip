<?php

namespace App\Http\Controllers;

use App\Services\PackingSlip\Repository\OrderRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class PackingSlip extends Controller
{
    public function __invoke(Request $request, OrderRepository $repository): Factory|\Illuminate\Foundation\Application|View|Application
    {
        return view('packing_slip', [
            'order' => $repository->find(),
        ]);
    }
}
