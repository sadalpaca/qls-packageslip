<?php

namespace App\Http\Controllers;

use App\Services\PackingSlip\Repository\OrderRepository;
use Illuminate\Http\Request;

class Pdf extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request, OrderRepository $orderRepository)
    {
        $imageData = file_get_contents(app_path() . '/../tests/Fixtures/example.pdf.png');

        return view('pdf', [
            'order' => $orderRepository->find(),
            'image' => 'data:image/png;base64,'.base64_encode($imageData),
            'shipment' => []
        ]);
    }
}
