<?php

namespace App\Services\PackingSlip\Repository;

use App\Services\PackingSlip\Aggregate\OrderAggregate;
use App\Services\PackingSlip\DTO\BillingAddress;
use App\Services\PackingSlip\DTO\DeliveryAddress;
use App\Services\PackingSlip\DTO\Order;
use App\Services\PackingSlip\DTO\OrderLine;
use App\Services\SomeShop\SomeShopService;
use Illuminate\Support\Arr;

class OrderRepository
{
    public function __construct(private SomeShopService $someShopService) {}

    public function find(): OrderAggregate
    {
        $order = $this->someShopService->get();

        $aggregate = new OrderAggregate(
            new Order(
                number: Arr::get($order, 'number'),
            ),
            new BillingAddress(
                companyName: Arr::get($order, 'billing_address.companyname'),
                name: Arr::get($order, 'billing_address.name', ''),
                street: Arr::get($order, 'billing_address.street', ''),
                houseNumber: Arr::get($order, 'billing_address.housenumber', ''),
                addressLine2: Arr::get($order, 'billing_address.address_line_2', ''),
                zipcode: Arr::get($order, 'billing_address.zipcode', ''),
                city: Arr::get($order, 'billing_address.city', ''),
                country: Arr::get($order, 'billing_address.country', ''),
                email: Arr::get($order, 'billing_address.email', ''),
                phone: Arr::get($order, 'billing_address.phone', '')
            ),
            new DeliveryAddress(
                companyName: Arr::get($order, 'delivery_address.companyname'),
                name: Arr::get($order, 'delivery_address.name', ''),
                street: Arr::get($order, 'delivery_address.street', ''),
                houseNumber: Arr::get($order, 'delivery_address.housenumber', ''),
                addressLine2: Arr::get($order, 'delivery_address.address_line_2', ''),
                zipcode: Arr::get($order, 'delivery_address.zipcode', ''),
                city: Arr::get($order, 'delivery_address.city', ''),
                country: Arr::get($order, 'delivery_address.country', ''),
            )
        );

        collect(Arr::get($order, 'order_lines', []))
            ->each(function (array $orderLine) use ($aggregate) {
                $aggregate->addOrderLine(
                    new OrderLine(
                        amountOrdered: Arr::get($orderLine, 'amount_ordered', 0),
                        name: Arr::get($orderLine, 'name', ''),
                        sku: Arr::get($orderLine, 'sku', 0),
                        ean: Arr::get($orderLine, 'ean', ''),
                    )
                );
            });

        return $aggregate;
    }
}
