<?php

namespace App\Services\PackingSlip\Builders;

use App\Services\PackingSlip\Aggregate\OrderAggregate;
use App\Services\PackingSlip\Repository\OrderRepository;
use App\Services\Qls\DTO\ReceiverContact;
use App\Services\Qls\DTO\ShipmentRequest;
use App\Services\Qls\Services\ImageService;
use App\Services\Qls\Services\ParcelService;
use App\Services\Qls\Services\PdfService;
use Illuminate\Support\Facades\Storage;

class PackageSlipBuilder
{
    public function __construct(
        private OrderRepository $orderRepository,
        private ParcelService $parcelService,
        private PdfService $pdfService,
        private ImageService $imageService
    ) {}

    public function build(): array
    {
        $order = $this->orderRepository->find();
        $shipment = $this->createShipmentRequest($order);
        $shipmentResponse = $this->parcelService->createShipment(config('qls.company_id'), $shipment);
        $pdfDownload = $this->pdfService->download($shipmentResponse->labels['a4']->offset0);

        $image = $this->imageService->pdf2image($pdfDownload->file);

        $this->imageService->crop($image->file);

        // Cleanup, again dirty ran out of time
        Storage::disk('local')->delete($pdfDownload->file);
        $imageData = Storage::disk('local')->get($image->file);

        return [
            'order' => $order,
            'shipment' => $shipment,
            'image' => 'data:image/png;base64,'.base64_encode($imageData),
        ];
    }

    private function createShipmentRequest(OrderAggregate $order): ShipmentRequest
    {
        return new ShipmentRequest(
            brandId: config('qls.brand_id'),
            reference: $order->getOrderNumber(),
            weight: 1000, // Should be calculated
            productId: 2,
            productCombinationId: 3,
            codAmount: 0,
            pieceTotal: 1,
            receiverContact: new ReceiverContact(
                companyName: $order->getDeliveryAddress()->companyName,
                name: $order->getDeliveryAddress()->name,
                street: $order->getDeliveryAddress()->street,
                houseNumber: $order->getDeliveryAddress()->houseNumber,
                postalCode: $order->getDeliveryAddress()->zipcode,
                locality: $order->getDeliveryAddress()->city,
                country: $order->getDeliveryAddress()->country,
                email: $order->getBillingAddress()->email
            ),
        );
    }
}
