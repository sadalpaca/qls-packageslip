<?php

namespace App\Services\PackingSlip\Aggregate;

use App\Services\PackingSlip\DTO\BillingAddress;
use App\Services\PackingSlip\DTO\DeliveryAddress;
use App\Services\PackingSlip\DTO\Order;
use App\Services\PackingSlip\DTO\OrderLine;

class OrderAggregate
{
    public function __construct(
        private readonly Order $root,
        private readonly BillingAddress $billingAddress,
        private readonly DeliveryAddress $deliveryAddress,
        private array $orderLines = []
    ) {}

    public function getRoot(): Order
    {
        return $this->root;
    }

    public function getOrderLines(): array
    {
        return $this->orderLines;
    }

    public function getDeliveryAddress(): DeliveryAddress
    {
        return $this->deliveryAddress;
    }

    public function getBillingAddress(): BillingAddress
    {
        return $this->billingAddress;
    }

    public function getOrderNumber(): string
    {
        return $this->root->number;
    }

    public function addOrderLine(OrderLine $line): void
    {
        $this->orderLines[] = $line;
    }

    public function toArray()
    {
        return [
            'number' => $this->root->number,
            'billing_address' => $this->billingAddress->toArray(),
            'delivery_address' => $this->deliveryAddress->toArray(),
            'order_lines' => collect($this->orderLines)->map(fn (OrderLine $orderLine) => $orderLine->toArray()),
        ];
    }
}
