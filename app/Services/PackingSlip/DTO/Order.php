<?php

namespace App\Services\PackingSlip\DTO;

readonly class Order
{
    public function __construct(
        public string $number
    ) {}
}
