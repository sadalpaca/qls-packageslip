<?php

namespace App\Services\PackingSlip\DTO;

readonly class OrderLine
{
    public function __construct(
        public int $amountOrdered,
        public string $name,
        public int $sku,
        public string $ean,
    ) {}

    public function toArray(): array
    {
        return [
            'amountOrdered' => $this->amountOrdered,
            'name' => $this->name,
            'sku' => $this->sku,
            'ean' => $this->ean,
        ];
    }
}
