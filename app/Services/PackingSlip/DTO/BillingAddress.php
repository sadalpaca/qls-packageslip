<?php

namespace App\Services\PackingSlip\DTO;

readonly class BillingAddress
{
    public function __construct(
        public ?string $companyName,
        public string $name,
        public string $street,
        public string $houseNumber,
        public string $addressLine2,
        public string $zipcode,
        public string $city,
        public string $country,
        public string $email,
        public string $phone,
    ) {}

    public function toArray(): array
    {
        return [
            'companyName' => $this->companyName,
            'name' => $this->name,
            'street' => $this->street,
            'houseNumber' => $this->houseNumber,
            'addressLine2' => $this->addressLine2,
            'zipcode' => $this->zipcode,
            'city' => $this->city,
            'country' => $this->country,
            'email' => $this->email,
            'phone' => $this->phone,
        ];
    }
}
