<?php

namespace App\Services\Qls\Exceptions;

use Exception;

class ImageCropFailedException extends Exception {}
