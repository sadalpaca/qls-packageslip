<?php

namespace App\Services\Qls\Exceptions;

use Exception;

class DownloadUrlEmptyException extends Exception {}
