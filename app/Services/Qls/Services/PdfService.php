<?php

namespace App\Services\Qls\Services;

use App\Services\Qls\Exceptions\DownloadUrlEmptyException;
use Illuminate\Support\Facades\Storage;
use Tests\Unit\Qls\DTO\PdfDownload;

class PdfService
{
    /**
     * @throws DownloadUrlEmptyException
     */
    public function download(string $source): PdfDownload
    {
        if (empty($source)) {
            throw new DownloadUrlEmptyException();
        }

        $pdfDownload = new PdfDownload();

        Storage::disk('local')->put($pdfDownload->file, file_get_contents($source));

        return $pdfDownload;
    }
}
