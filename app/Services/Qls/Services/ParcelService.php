<?php

namespace App\Services\Qls\Services;

use App\Services\Qls\DTO\Product;
use App\Services\Qls\DTO\ShipmentRequest;
use App\Services\Qls\DTO\ShipmentResponse;
use App\Services\Qls\Exceptions\ProductsNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

class ParcelService
{
    /**
     * @throws ProductsNotFoundException
     */
    public function products(string $companyId): Collection
    {
        $response = Http::qls()->get('/companies/'.$companyId.'/products');

        if (! $response->successful()) {
            throw new ProductsNotFoundException();
        }

        return collect($response->json('data'))->map(function ($item) {
            return Product::fromArray($item);
        });
    }

    public function createShipment(string $companyId, ShipmentRequest $request): ShipmentResponse
    {
        $response = Http::qls()->post('/company/'.$companyId.'/shipment/create', $request->toArray());

        return ShipmentResponse::fromArray($response->json('data'));
    }
}
