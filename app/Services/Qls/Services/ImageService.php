<?php

namespace App\Services\Qls\Services;

use App\Services\Qls\DTO\Image;
use App\Services\Qls\Exceptions\ImageCropFailedException;
use App\Services\Qls\Exceptions\Pdf2ImageNotSucceededException;
use Illuminate\Support\Facades\Process;
use Illuminate\Support\Facades\Storage;

class ImageService
{
    /**
     * @throws Pdf2ImageNotSucceededException
     */
    public function pdf2image(string $filename): Image
    {
        $disk = Storage::disk('local');

        $file = $disk->path($filename);
        $targetFilename = $filename.'.png';
        $target = $disk->path($targetFilename);

        $result = Process::run('pdftoppm -singlefile -png '.$file.' > '.$target);

        if (! $result->successful()) {
            throw new Pdf2ImageNotSucceededException();
        }

        return new Image(file: $targetFilename);
    }

    /**
     * @throws ImageCropFailedException
     */
    public function crop(string $filename): bool
    {
        $disk = Storage::disk('local');
        $file = $disk->path($filename);

        $result = Process::run(sprintf('convert %s -trim %s', $file, $file));

        if (! $result->successful()) {
            throw new ImageCropFailedException();
        }

        return true;
    }
}
