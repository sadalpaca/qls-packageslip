<?php

namespace App\Services\Qls\DTO;

use Illuminate\Support\Arr;

readonly class Product
{
    public function __construct(
        public int $id,
        public ?string $name) {}

    public static function fromArray(array $json): Product
    {
        return new self(
            id: Arr::get($json, 'id', 0),
            name: Arr::get($json, 'name'),
        );
    }
}
