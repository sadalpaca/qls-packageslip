<?php

namespace App\Services\Qls\DTO;

readonly class ReceiverContact
{
    public function __construct(
        public ?string $companyName,
        public string $name,
        public string $street,
        public string $houseNumber,
        public string $postalCode,
        public string $locality,
        public string $country,
        public string $email,
    ) {}

    public function toArray(): array
    {
        return [
            'companyname' => $this->companyName,
            'name' => $this->name,
            'street' => $this->street,
            'housenumber' => $this->houseNumber,
            'postalcode' => $this->postalCode,
            'locality' => $this->locality,
            'country' => $this->country,
            'email' => $this->email,
        ];
    }
}
