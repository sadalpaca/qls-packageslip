<?php

namespace App\Services\Qls\DTO;

use Illuminate\Support\Arr;

class Label
{
    public function __construct(
        public ?string $offset0,
        public ?string $offset1,
        public ?string $offset2,
        public ?string $offset3,
        public ?string $default,
    ) {}

    public static function fromArray(array $label): Label
    {
        return new self(
            offset0: Arr::get($label, 'offset_0'),
            offset1: Arr::get($label, 'offset_1'),
            offset2: Arr::get($label, 'offset_2'),
            offset3: Arr::get($label, 'offset_3'),
            default: Arr::get($label, 0, Arr::get($label, 'offset_0'))
        );
    }

    public function __toString(): string
    {
        return $this->default;
    }
}
