<?php

namespace App\Services\Qls\DTO;

readonly class ShipmentRequest
{
    public function __construct(
        public string $brandId,
        public string $reference,
        public float $weight,
        public string $productId,
        public string $productCombinationId,
        public int $codAmount,
        public int $pieceTotal,
        public ReceiverContact $receiverContact,
    ) {}

    public function toArray(): array
    {
        return [
            'brand_id' => $this->brandId,
            'reference' => $this->reference,
            'weight' => $this->weight,
            'product_id' => $this->productId,
            'product_combination_id' => $this->productCombinationId,
            'cod_amount' => $this->codAmount,
            'piece_total' => $this->pieceTotal,
            'receiver_contact' => $this->receiverContact->toArray(),
        ];
    }
}
