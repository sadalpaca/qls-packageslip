<?php

namespace App\Services\Qls\DTO;

use Illuminate\Support\Arr;

readonly class Shipment
{
    public function __construct(
        public string $id,
        public string $barcode,
    ) {}

    public static function fromArray(array $array): Shipment
    {
        return new self(
            id: Arr::get($array, 'id', 0),
            barcode: Arr::get($array, 'barcode', ''),
        );
    }
}
