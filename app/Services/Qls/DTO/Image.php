<?php

namespace App\Services\Qls\DTO;

readonly class Image
{
    public function __construct(
        public string $file) {}
}
