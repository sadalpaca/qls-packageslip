<?php

namespace App\Services\Qls\DTO;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

readonly class ShipmentResponse
{
    public function __construct(
        public readonly string $id,
        public readonly Collection $shipments,
        public readonly Collection $labels,
    ) {}

    public static function fromArray(array $array): ShipmentResponse
    {
        return new self(
            id: Arr::get($array, 'id'),
            shipments: collect(Arr::get($array, 'shipments', []))->each(function ($shipment) {
                return Shipment::fromArray($shipment);
            }),
            labels: collect(Arr::get($array, 'labels', []))->mapWithKeys(function ($label, $key) {
                return [$key => Label::fromArray(Arr::wrap($label))];
            })
        );
    }
}
