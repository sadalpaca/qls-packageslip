<?php

namespace App\Providers;

use App\Services\Qls\Services\ImageService;
use App\Services\Qls\Services\ParcelService;
use App\Services\Qls\Services\PdfService;
use App\Services\SomeShop\SomeShopService;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(ParcelService::class, function ($app) {
            return new ParcelService();
        });

        $this->app->singleton(PdfService::class, function ($app) {
            return new PdfService();
        });

        $this->app->singleton(ImageService::class, function ($app) {
            return new ImageService();
        });

        $this->app->singleton(SomeShopService::class, function ($app) {
            return new SomeShopService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Http::macro('qls', function () {
            return Http::withHeaders([
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ])
                ->withBasicAuth(config('qls.api')['user'], config('qls.api')['password'])
                ->baseUrl(config('qls.api')['host']);
        });
    }
}
