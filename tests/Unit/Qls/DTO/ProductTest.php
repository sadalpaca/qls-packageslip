<?php

use App\Services\Qls\DTO\Product;

it('accepts an array and returns a product', function () {
    // Most likely this can be done way nicer ;-)
    $response = json_decode(file_get_contents(app_path().'/../tests/Fixtures/products.json'), true);

    foreach ($response['data'] as $item) {
        $product = Product::fromArray($item);
        expect($product)
            ->toBeInstanceOf(Product::class)
            ->id->toBe($item['id'])
            ->name->toBe($item['name']);
    }
});
