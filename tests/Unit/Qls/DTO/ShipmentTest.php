<?php

use App\Services\Qls\DTO\Shipment;

it('accepts an array and returns a shipment with labels', function () {
    // Most likely this can be done way nicer ;-)
    $response = json_decode(file_get_contents(app_path().'/../tests/Fixtures/shipment.json'), true);

    $shipment = $response['data']['shipments'][0];

    $product = Shipment::fromArray($shipment);
    expect($product)
        ->toBeInstanceOf(Shipment::class)
        ->id->toBe($shipment['id'])
        ->barcode->toBe($shipment['barcode']);
});
