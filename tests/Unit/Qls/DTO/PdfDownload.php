<?php

namespace Tests\Unit\Qls\DTO;

use Ramsey\Uuid\Uuid;

readonly class PdfDownload
{
    public readonly string $id;

    public readonly string $file;

    public function __construct(
    ) {
        $this->id = Uuid::uuid4();
        $this->file = $this->id.'.pdf';
    }
}
