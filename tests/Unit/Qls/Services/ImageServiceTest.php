<?php

use App\Services\Qls\DTO\Image;
use App\Services\Qls\Exceptions\ImageCropFailedException;
use App\Services\Qls\Exceptions\Pdf2ImageNotSucceededException;
use App\Services\Qls\Services\ImageService;
use Illuminate\Support\Facades\Storage;

it('can convert a pdf to an image and can crop it', function () {
    Storage::fake('local');

    $file = base_path().'/tests/Fixtures/example-label.pdf';

    Storage::disk('local')->put('test.pdf', file_get_contents($file));
    Storage::disk('local')->assertExists('test.pdf');

    $imageService = $this->app->get(ImageService::class);

    $image = $imageService->pdf2image('test.pdf');

    expect($image)->toBeInstanceOf(Image::class)
        ->and($imageService->crop($image->file))->toBeTrue();
});

it('throws an exception when trying to convert a non-existing pdf to an image', function () {
    $imageService = $this->app->get(ImageService::class);

    expect(fn () => $imageService->pdf2image('does-not-exist.pdf'))
        ->toThrow(Pdf2ImageNotSucceededException::class);
});

it('throws an exception when trying to crop an image', function () {
    $imageService = $this->app->get(ImageService::class);

    expect(fn () => $imageService->crop('does-not-exist.pdf'))
        ->toThrow(ImageCropFailedException::class);
});
