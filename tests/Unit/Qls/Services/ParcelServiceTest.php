<?php

use App\Services\Qls\DTO\Label;
use App\Services\Qls\DTO\Product;
use App\Services\Qls\DTO\ReceiverContact;
use App\Services\Qls\DTO\ShipmentRequest;
use App\Services\Qls\DTO\ShipmentResponse;
use App\Services\Qls\Exceptions\ProductsNotFoundException;
use App\Services\Qls\Services\ParcelService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;

it('will fetch data and return a collection of products', function () {
    $response = json_decode(file_get_contents(app_path().'/../tests/Fixtures/products.json'), true);

    $parcelApiService = $this->app->get(ParcelService::class);

    Http::fake([
        'companies/*' => Http::response($response, 200),
    ]);

    $response = $parcelApiService->products(config('qls.company_id'));

    expect($response)
        ->toBeInstanceOf(Collection::class)
        ->toContainOnlyInstancesOf(Product::class);
});

it('can create a new shipment and return the appropriate data as a model', function () {
    $response = json_decode(file_get_contents(app_path().'/../tests/Fixtures/shipment.json'), true);

    Http::fake([
        'company/*' => Http::response($response, 200),
    ]);

    $request = new ShipmentRequest(
        brandId: config('qls.brand_id'),
        reference: '#958201',
        weight: 1000,
        productId: 2,
        productCombinationId: 3,
        codAmount: 0,
        pieceTotal: 1,
        receiverContact: new ReceiverContact(
            companyName: null,
            name: 'John Doe',
            street: 'Daltonstraat',
            houseNumber: '65',
            postalCode: '3316GD',
            locality: 'Dordrecht',
            country: 'NL',
            email: 'email@example.com'
        )
    );

    $parcelApiService = $this->app->get(ParcelService::class);
    $response = $parcelApiService->createShipment(config('qls.company_id'), $request);

    expect($response)
        ->toBeInstanceOf(ShipmentResponse::class)
        ->labels->toContainOnlyInstancesOf(Label::class)
        ->shipments->toBeInstanceOf(Collection::class);
});

it('throws an exception when products cannot be found', function () {
    $parcelApiService = $this->app->get(ParcelService::class);

    Http::fake([
        'companies/*' => Http::response('', 404),
    ]);

    expect(fn () => $parcelApiService->products(config('qls.company_id')))
        ->toThrow(ProductsNotFoundException::class);
});
