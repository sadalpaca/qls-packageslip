<?php

use App\Services\Qls\DTO\Label;
use App\Services\Qls\Exceptions\DownloadUrlEmptyException;
use App\Services\Qls\Services\PdfService;
use Illuminate\Support\Facades\Storage;

it('can download a pdf', function () {
    Storage::fake('disk');

    $label = Label::fromArray(
        [
            'offset_0' => base_path().'/tests/Fixtures/example-label.pdf',
        ]
    );

    $pdfService = $this->app->get(PdfService::class);

    $pdfDownload = $pdfService->download((string) $label);

    Storage::disk('local')->assertExists($pdfDownload->file);
});

it('throws an exception when trying to download a pdf with an empty file target', function () {
    $pdfService = $this->app->get(PdfService::class);

    expect(fn () => $pdfService->download(''))
        ->toThrow(DownloadUrlEmptyException::class);
});
