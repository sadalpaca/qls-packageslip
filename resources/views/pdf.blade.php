<x-layout>
    <header>
        <h1>QLS fulfilment</h1>
    </header>

    <main class="container mx-auto">
        <div class="text-center">
            <h1 class="text-xl font-bold">PAKBON</h1>
        </div>

        <div class="mt-6">
            <table class="border-collapse table-fixed text-sm">
                <tbody>
                <tr>
                    <th class="text-left">Verzenddatum:</th>
                    <td class="text-left">28/06/2024</td>
                </tr>
                <tr>
                    <th class="text-left">Bestelnummer:</th>
                    <td class="text-left">{{ $order->getOrderNumber() }}</td>
                </tr>
                </tbody>
            </table>
        </div>


        <div class="mt-6">
            <table class="border-collapse table-auto w-full text-sm">
                <thead>
                <tr>
                    <th class="text-center">Aantal</th>
                    <th class="text-center">Code</th>
                    <th class="text-center">Omschrijving</th>
                </tr>
                </thead>
                <tbody>
                @foreach($order->getOrderLines() as $line)
                    <tr>
                        <td class="text-center">{{ $line->amountOrdered }}</td>
                        <td class="text-center">{{ $line->sku  }}</td>
                        <td class="text-center">{{ $line->name  }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="mt-12 text-center">
            <span>Controleer goed of de bestelling compleet is</span>
        </div>

        <div class="mt-12">
            <img width="300" src="{{ $image }}" alt="packing slip">
        </div>
    </main>
</x-layout>
