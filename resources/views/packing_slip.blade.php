<x-layout>
    <div class="container mx-auto px-4">
        Creating package slip for:

        <h2>{{ $order->getOrderNumber() }}</h2>

        <table class="table table-auto w-3/4">
            <thead>
            <tr>
                <th class="text-left">Name</th>
                <th class="text-left">Amount ordered</th>
                <th class="text-left">SKU</th>
                <th class="text-left">EAN</th>
            </tr>
            </thead>

            @foreach($order->getOrderLines() as $line)
                <tbody>
                    <tr>
                        <td>
                            {{ $line->name }}
                        </td>
                        <td>
                            {{ $line->amountOrdered }}
                        </td>
                        <td>
                            {{ $line->sku }}
                        </td>
                        <td>
                            {{ $line->ean }}
                        </td>
                    </tr>
                </tbody>
            @endforeach

        </table>

        <form method="post" action="{{ route('generate-packaging-slip')  }}">
            @csrf
            <div class="mt-4">
                Select shipping method and options
            </div>

            <div class="mt-4">
                <label>DHL Pakje (NL)</label>
                <input
                    type="radio"
                    checked="checked"
                    name="product"
                    value="2"
                    class="border-gray-300 text-indigo-600 shadow-sm focus:ring-indigo-500 ml-3"
                />
            </div>

            <div class="mt-4">
                <label>DHL Pakje (NL) without delivery options</label>
                <input
                    type="radio"
                    checked="checked"
                    name="productCombination"
                    value="3"
                    class="border-gray-300 text-indigo-600 shadow-sm focus:ring-indigo-500 ml-3"
                />
            </div>


            <div class="mt-4">
                <button
                    class="px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 focus:bg-gray-700 active:bg-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 transition ease-in-out duration-150"
                >
                    Generate packaging slip
                </button>
            </div>
        </form>
    </div>
</x-layout>
