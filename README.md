# Introduction

To create this test I have opted for clean laravel install with minimal extras:

- No Starter Kit; Meaning CSS (TailwindCSS) is loaded as-is from the default Laravel install. Vite is not installed
- Pest; Never used it, so seems like a great time to check it out, looks a lot like Ruby On Rails testing, so cool.
- Gotenberg; Used for PDF generation
- Custom Dockerfile; Adds pdf to image tool and imagemagick for cropping
- Pint for opinionated code styling

**Warning**

This all could have been done in a couple of lines of code with some docker tools.
However, I like to challenge myself with these tests. So it is a bit of an overengineered product ;-)

There are not many fallbacks and checks if processes are done correctly, simply put I ran out of time by choosing this
route.
As said, it could have been done in fewer lines of code with a lot less data juggling.

But I tried to showcase a bit of my knowledge with the risk of failing the test due to complexity or design choices.

# What could be done better

- The front-end is a mess, so that would need cleaning
- Proper checks if processes have failed and a way nicer package slip builder
- Nicer naming of classes in the `Services` folder
- Better and more complete testing
- Less fluff ;-)
- **Better commit messages, I just created the project and commited afterwards, not how I do things normally**

# Getting started

I used `sail` to quickly get up and running. You need Docker installed and at least PHP 8.3.
There is customization in the Dockerfile to support image and PDF processing. The docker build step is essential.

```shell
# Generate a simple alias, or replace all `sail` commands with `./vendor/bin/sail`
alias sail='sh $([ -f sail ] && echo sail || echo vendor/bin/sail)'

# Install dependencies and binaries for sail
composer install

# Build the custom container
sail build

# You will need to decrypt the env file to get started
# The decryption key can be found here for seven days: https://privnote.com/ALrvBe7u#U1pNDhbFV
sail artisan env:decrypt --key=base64:******


# Run sail, you can add -d but logging is present, I tend to start mine without in a separate terminal window
sail up
```

Furthermore, you can run the test suite (not fully tested):

```shell
sail test
```

