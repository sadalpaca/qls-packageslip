<?php

return [
    'api' => [
        'host' => env('QLS_API_HOST', 'localhost'),
        'user' => env('QLS_API_USER', 'user'),
        'password' => env('QLS_API_PASSWORD', 'password'),
    ],
    'company_id' => env('QLS_COMPANY_ID', null),
    'brand_id' => env('QLS_BRAND_ID', null),
];
