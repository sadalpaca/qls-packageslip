<?php

use App\Http\Controllers\GeneratePackagingSlip;
use App\Http\Controllers\PackingSlip;
use App\Http\Controllers\Pdf;
use Illuminate\Support\Facades\Route;

Route::get('/', PackingSlip::class);

Route::post(
    'generate-packaging-slip',
    GeneratePackagingSlip::class)
    ->name('generate-packaging-slip');

Route::get('pdf', Pdf::class);
